import cv2
import requests
import os
import shutil
import sys
import json
from multiprocessing.pool import ThreadPool
from random import randint

pool = ThreadPool(processes=10)
url = 'http://backend.clipapp.in/wibes'
#url = 'http://localhost:9000/wibes'
cd = os.path.dirname(os.path.abspath(__file__))

if os.path.exists(cd+"/TrimmedVideos"):
    shutil.rmtree(cd+"/TrimmedVideos")
os.makedirs(cd+"/TrimmedVideos")
videoDir = sys.argv[1]
uploaderId1= int(input("Enter starting UploaderId:"))
uploaderId2= int(input("Enter ending UploaderId:"))
if (uploaderId1 > uploaderId2):
    print("Starting UploaderId cannot be greater than ending UploaderId")
    sys.exit()
tag = input("Enter tag if any:")
videoFormat = input("Enter videoFormat:")

def uploadVideo(videoFile, width, height, count):
    uploaderId = randint(uploaderId1, uploaderId2)
    headers = {
    'secret': "Random Shit",
    'token': "28a06c72-12ef-46af-ad8b-670672e2f31a"
    }
    files = {'file': (videoFile, open(videoFile, 'rb'), 'video')}
    data = {"categories":"",
        "tags": tag,
        "ext":videoFormat,
        "height": height,
        "width": width,
        "uploaderId": uploaderId }
    print (data)
    r = requests.post(url, files=files, data=data, headers=headers)
    resp_dict = json.loads(r.text)
    if (resp_dict['status'] == True):
        os.remove(videoFile)
        #os.remove(videoFile.replace("TrimmedVideos","Videos"))
        wibeId = str(json.loads(resp_dict["message"])["wibeId"])
        return ("Uploaded "+videoFile+" with wibeId:"+wibeId)
    else:
        count= count+1;
        print (count)
        if (count < 5):
            return uploadVideo(videoFile, width, height, count)
        else:
            print (r.text)
            return "Upload failed for "+videoFile

def list_files(dir):
    r = []
    for root, dirs, files in os.walk(dir):
        for name in files:
            if name.endswith('.' + videoFormat):
                r.append(name)
    print (r)
    return r

def getVideoDimensions(video):
    vcap = cv2.VideoCapture(video)
    width = vcap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = vcap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    print ([int(width), int(height)])
    return [int(width), int(height)]

def createTrimmedVideos(video):
    for filename in os.listdir(video):
        if (filename.endswith(videoFormat)):
            #os.system("ffmpeg -y -i "+cd +"/"+videoDir+filename+" -ss 00:00:"+trimSeconds+" -c copy "+cd+"/TrimmedVideos/"+filename)
            shutil.copyfile(cd +"/"+videoDir+filename, cd+"/TrimmedVideos/"+filename)
        else:
            continue

results = []
def startUpload():
    createTrimmedVideos(cd +"/"+videoDir)
    videoFiles = list_files(cd +"/TrimmedVideos")
    for videoFile in videoFiles:
        dim = getVideoDimensions(cd +"/TrimmedVideos/"+videoFile);
        print ("Uploading============== "+videoFile)
        async_result = pool.apply_async(uploadVideo, (cd+"/TrimmedVideos/"+videoFile,dim[0],dim[1],0))
        results.append(async_result)
        #uploadVideo(cd+"/TrimmedVideos/"+videoFile, dim[0], dim[1], uploaderId)

startUpload()
for result in results:
    print (result.get())
