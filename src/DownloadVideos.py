import os
import subprocess
import time
import sys
import datetime
import json
import shutil
from multiprocessing.pool import ThreadPool
import csv
import sys
import traceback
from common_scripts import *

pool = ThreadPool(processes=10)
path= os.path.dirname(os.path.abspath(__file__)) + "/.."
if not(os.path.exists("auth.json")):
    print("Auth file does not exist")
    sys.exit()

print ("Enter 1 for downloading videos for all channels")
print ("Enter 2 for downloading videos for particular channels(Need to write down the channels in Channels.csv file)")

option = input("Choose a option:")

if (option == "1"):
    source = input("Enter source:")
    sourceList = ["mandir"]
    if (sourceList.count(source) == 0):
        print ("Invalid Source")
        sys.exit()

elif (option == "2"):
    source = input("Enter source:")
    sourceList = ["insta","shrcht"]

    if (sourceList.count(source) == 0):
        print("Invalid Source")
        sys.exit()

    if not(os.path.exists("Channels.csv")):
        print("Channel.csv file does not exist")
        sys.exit()

    with open ('Channels.csv', 'rt') as f:
        reader = csv.reader(f)
        channelList = [i[0] for i in reader]

else:
    print("Invalid option chosen")
    sys.exit()

now = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
dirName= source+"_"+now
lang= input("Enter language:")
def getLocale(x):
    return {
        'hindi': 'hi_IN',
        'kannada': 'kn_IN',
        'bengali': 'bn_IN',
        'gujarati': 'gu_IN',
        'malayalam': 'ml_IN',
        'marathi': 'mr_IN',
        'punjabi': 'pn_IN',
        'tamil': 'ta_IN',
        'telugu': 'te_IN',
        'urdu': 'ur_IN'
    }.get(x, "Invalid Language")

locale= getLocale(lang)
if (locale == "Invalid Language"):
    print (locale)
    sys.exit()

def validate(date_text):
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except ValueError:
        print("Incorrect date format, should be YYYY-MM-DD")
        sys.exit()

fromTime= input("Enter from timestamp:")
toTime= input("Enter to timestamp:")

os.makedirs(path+"/"+dirName)
results = []

if (option == "1"):
    arr = urlRespBody(source,locale, fromTime, toTime, "NA")
    channelsStr = 'channelsStr'
    videoUrl = 'videoUrl'
    for item in arr:
        if (channelsStr in item and videoUrl in item):
            key = item[channelsStr]
            if (key == ""):
                clnList= ["untagged"]
            else:
                list = key.split("||")
                clnList = map(lambda s: s.strip(),list)
            for channel in clnList:
                videoList = []
                channelPath = path+"/"+dirName+"/"+channel
                if not(os.path.exists(channelPath)):
                    os.makedirs(channelPath)
                vidUrl = item[videoUrl]
                print (vidUrl)
                async_result = pool.apply_async(downloadUrl, (channelPath, vidUrl, source))
                results.append(async_result)

else:
    for channel in channelList:
        arr = urlRespBody(source,locale, fromTime, toTime, channel)
        channelPath = path+"/"+dirName+"/"+channel
        if not(os.path.exists(channelPath)):
            os.makedirs(channelPath)
        results = []
        videoList = []
        videoPage = 'videoPage'
        videoUrl = 'videoUrl'
        for item in arr:
            if (videoUrl in item):
                vidUrl = item[videoUrl]
                #videoList.append(item[videoUrl])
            elif (videoPage in item):
                try:
                    vidUrl = subprocess.check_output(['youtube-dl','-g',item[videoPage]]).decode("utf-8").rstrip()
                    #print("videoPage URLS----------->>>"+vidUrl)
                except:
                    traceback.print_exc()
                    print ("Gave error:"+item[videoPage])
            else:
                continue
            async_result = pool.apply_async(downloadUrl, (channelPath, vidUrl, source))
            results.append(async_result)

for result in results:
    print(result.get())

print ("Starting creation of All_Videos directory")
allVideosDir = path+"/"+dirName+"/All_Videos"
os.makedirs(allVideosDir)
RootDir = path+"/"+dirName
TargetFolder = allVideosDir
for root, dirs, files in os.walk((os.path.normpath(RootDir)), topdown=False):
        for name in files:
            if (os.path.exists(TargetFolder+"/"+name) == False):
                SourceFolder = os.path.join(root,name)
                shutil.copy2(SourceFolder, TargetFolder)
