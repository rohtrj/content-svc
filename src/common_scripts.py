import json
import wget
import traceback
import uuid
import requests


def urlRespBody(source,locale, fromTime, toTime, channel):
    if (channel == "NA"):
        url = "https://content-svc.prod.plystash.tech/v1/post-content/source/name/"+source+"/lang/"+locale+"/fromTime/"+fromTime+"/toTime/"+toTime
    else:
        url = "https://content-svc.prod.plystash.tech/v1/post-content/source/name/"+source+"/lang/"+locale+"/fromTime/"+fromTime+"/toTime/"+toTime+"/channel/"+channel
    print(url)
    with open("auth.json") as file:
        auth = json.loads(file.read())
    headers = auth
    r = requests.get(url, headers=headers, verify=False)
    r.encoding = 'utf-8'
    arr = json.loads(r.text)
    return arr

def downloadUrl(channelPath,vidUrl,src):
    resp=[]
    if (src == "shrcht"):
        vidUrl = vidUrl.replace("watermarked", "compressed")
    print("Download Started for video: "+vidUrl)
    filename = str(uuid.uuid4())+".mp4"
    try:
        wget.download(vidUrl,channelPath+"/"+filename)
    except:
        traceback.print_exc()
        print ("Download Failed for: "+vidUrl)
        resp.append("Failure: "+vidUrl)
    print("Download Completed for video:"+vidUrl)
    #resp.append("Success: "+vidUrl)
    return resp
